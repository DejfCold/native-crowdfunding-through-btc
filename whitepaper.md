# Native crowdfunding through BTC blockchain

## Use case
Distributed P2P crowdfunding

## Requirements
1. Someone, possibly the person who requires the money or some bystander on behalf of that person creates a crowdfund and sets funds release conditions, expected crowdfund duration and optionally an arbiter
2. Multiple, possibly many, wallets allocate and lock some funds for the purpose of crowdfunding to that crowd fund
3A) When the conditions are met within the expected crowdfund duration, funds are released to the donee
3B) When the conditions aren't met within the expected crowdfund duration, funds are unlocked and return to the original donors

## Need for arbitration
The release conditions may or may not be all evaluable by the computer. Also the required data may not be present on chain and instead a URL and a fingerprint only may be present.
For these reasons there might be required an (distributed) arbiter that would check these conditions and report back their status.

## Diagram
```plantuml
object block1 {
  create crowdfund
}
object block2 {
  add funds
}
object block3 {
  add funds
}
object block4 {
  evaluate conditions
}
object arbiter {
  evaluate conditions
}
object block5A {
  release funds to donors
}
object block5B {
  release funds to donee
}

block1 -> block2
block2 -> block3
block3 -> block4
block4 --> block5A : fails

block4 --> arbiter : passes

arbiter --> block5A : rules in favor of donors
arbiter --> block5B : rules in favor of donee
```
